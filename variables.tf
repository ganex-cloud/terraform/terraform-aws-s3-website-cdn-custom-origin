variable "name" {
  description = "Name of s3 bucket"
}

variable "force_destroy" {
  description = "Delete all objects in bucket on destroy"
  default     = false
}

variable "versioned" {
  description = "Version the bucket"
  default     = false
}

variable "policy" {
  default     = ""
  description = "A valid bucket policy JSON document"
}

variable "tags" {
  type        = "map"
  description = "Additional Tags"
  default     = {}
}

variable "acl" {
  description = "ACL"
  default     = "private"
}

variable "website" {
  description = "Map containing static web-site hosting or redirect configuration."
  type        = "list"

  default = [
    {
      index_document = "index.html"
      error_document = "404.html"
    },
  ]
}

variable "cloudfront_enabled" {
  description = "Enable Cloudfront"
  default     = "false"
}

variable "cloudfront_is_ipv6_enabled" {
  default     = "true"
  description = "State of CloudFront IPv6"
}

variable "cloudfront_index_document" {
  description = "Configure index"
  default     = "index.html"
}

variable "cloudfront_allowed_methods" {
  type        = "list"
  default     = ["GET", "HEAD"]
  description = "List of allowed methods (e.g. ` GET, PUT, POST, DELETE, HEAD`) for AWS CloudFront"
}

variable "cloudfront_cached_methods" {
  type        = "list"
  default     = ["GET", "HEAD"]
  description = "List of cached methods (e.g. ` GET, PUT, POST, DELETE, HEAD`)"
}

variable "cloudfront_distribution_name" {
  description = "The name of the distribution."
  type        = "string"
  default     = ""
}

variable "cloudfront_comment" {
  description = "Cloudfront comments"
  type        = "string"
  default     = ""
}

variable "cloudfront_forward_query_string" {
  default     = "false"
  description = "Forward query strings to the origin that is associated with this cache behavior"
}

variable "cloudfront_forward_headers" {
  description = "Specifies the Headers, if any, that you want CloudFront to vary upon for this cache behavior. Specify `*` to include all headers."
  type        = "list"
  default     = []
}

variable "cloudfront_forward_cookies" {
  description = "Specifies whether you want CloudFront to forward cookies to the origin. Valid options are all, none or whitelist"
  default     = "none"
}

variable "cloudfront_forward_cookies_whitelisted_names" {
  type        = "list"
  description = "List of forwarded cookie names"
  default     = []
}

variable "cloudfront_default_ttl" {
  default     = "3600"
  description = "Default amount of time (in seconds) that an object is in a CloudFront cache"
}

variable "cloudfront_min_ttl" {
  default     = "0"
  description = "Minimum amount of time that you want objects to stay in CloudFront caches"
}

variable "cloudfront_max_ttl" {
  default     = "86400"
  description = "Maximum amount of time (in seconds) that an object is in a CloudFront cache"
}

variable "cloudfront_geo_restriction_type" {
  default     = "none"
  description = "Method that use to restrict distribution of your content by country: `none`, `whitelist`, or `blacklist`"
}

variable "cloudfront_geo_restriction_locations" {
  type        = "list"
  default     = []
  description = "List of country codes for which  CloudFront either to distribute content (whitelist) or not distribute your content (blacklist)"
}

variable "cloudfront_aliases" {
  type        = "list"
  description = "List of cloudfront_aliases"
  default     = [""]
}

variable "cloudfront_viewer_protocol_policy" {
  description = "Protocol Policy"
  default     = ""
}

variable "cloudfront_origin_domain_name" {
  description = "(Required) - The DNS domain name of your custom origin (e.g. website)"
  default     = ""
}

variable "cloudfront_origin_path" {
  description = "(Optional) - An optional element that causes CloudFront to request your content from a directory in your Amazon S3 bucket or your custom origin"
  default     = ""
}

variable "cloudfront_origin_http_port" {
  description = "(Required) - The HTTP port the custom origin listens on"
  default     = "80"
}

variable "cloudfront_origin_https_port" {
  description = "(Required) - The HTTPS port the custom origin listens on"
  default     = "443"
}

variable "cloudfront_origin_protocol_policy" {
  description = "(Required) - The origin protocol policy to apply to your origin. One of http-only, https-only, or match-viewer"
  default     = "match-viewer"
}

variable "cloudfront_origin_ssl_protocols" {
  description = "(Required) - The SSL/TLS protocols that you want CloudFront to use when communicating with your origin over HTTPS"
  type        = "list"
  default     = ["TLSv1", "TLSv1.1", "TLSv1.2"]
}

variable "cloudfront_origin_keepalive_timeout" {
  description = "(Optional) The Custom KeepAlive timeout, in seconds. By default, AWS enforces a limit of 60. But you can request an increase."
  default     = "60"
}

variable "cloudfront_origin_read_timeout" {
  description = "(Optional) The Custom Read timeout, in seconds. By default, AWS enforces a limit of 60. But you can request an increase."
  default     = "60"
}

variable "cloudfront_compress" {
  description = "(Optional) Whether you want CloudFront to automatically compress content for web requests that include Accept-Encoding: gzip in the request header (default: false)"
  default     = "false"
}

variable "cloudfront_default_root_object" {
  default     = "index.html"
  description = "Object that CloudFront return when requests the root URL"
}

variable "cloudfront_price_class" {
  default     = "PriceClass_All"
  description = "Price class for this distribution: `PriceClass_All`, `PriceClass_200`, `PriceClass_100`"
}

variable "route53_enabled" {
  type        = "string"
  default     = "true"
  description = "Set to false to prevent the module from creating any resources"
}

variable "route53_parent_zone_id" {
  default     = ""
  description = "ID of the hosted zone to contain this record  (or specify `parent_zone_name`)"
}

variable "route53_parent_zone_name" {
  default     = ""
  description = "Name of the hosted zone to contain this record (or specify `parent_zone_id`)"
}

variable "route53_evaluate_target_health" {
  default     = "false"
  description = "Set to true if you want Route 53 to determine whether to respond to DNS queries"
}

variable "acm_certificate_arn" {
  description = "ARN of Certificate"
  default     = ""
}

variable "cloudfront_error_caching_min_ttl" {
  description = "TTL to caching error"
  default     = ""
}

variable "cloudfront_error_code" {
  description = "Cloudfront error code"
  default     = ""
}

variable "cloudfront_response_code" {
  description = "Cloudfront response code"
  default     = ""
}

variable "cloudfront_response_page_path" {
  description = "Cloudfront response page path"
  default     = ""
}
