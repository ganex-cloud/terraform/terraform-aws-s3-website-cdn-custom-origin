resource "aws_s3_bucket" "name" {
  bucket        = "${var.name}"
  force_destroy = "${var.force_destroy}"
  tags          = "${var.tags}"
  policy        = "${var.policy}"
  acl           = "${var.acl}"

  website = ["${var.website}"]

  versioning {
    enabled = "${var.versioned}"
  }
}

resource "aws_cloudfront_distribution" "default" {
  count               = "${var.cloudfront_enabled == "true" ? 1 : 0}"
  enabled             = true
  is_ipv6_enabled     = "${var.cloudfront_is_ipv6_enabled}"
  comment             = "${var.cloudfront_comment}"
  default_root_object = "${var.cloudfront_index_document}"
  aliases             = ["${var.cloudfront_aliases}"]
  price_class         = "${var.cloudfront_price_class}"

  default_cache_behavior {
    allowed_methods  = "${var.cloudfront_allowed_methods}"
    cached_methods   = "${var.cloudfront_cached_methods}"
    target_origin_id = "${var.cloudfront_distribution_name}"
    compress         = "${var.cloudfront_compress}"

    forwarded_values {
      headers = ["${var.cloudfront_forward_headers}"]

      query_string = "${var.cloudfront_forward_query_string}"

      cookies {
        forward           = "${var.cloudfront_forward_cookies}"
        whitelisted_names = ["${var.cloudfront_forward_cookies_whitelisted_names}"]
      }
    }

    viewer_protocol_policy = "${var.cloudfront_viewer_protocol_policy}"
    default_ttl            = "${var.cloudfront_default_ttl}"
    min_ttl                = "${var.cloudfront_min_ttl}"
    max_ttl                = "${var.cloudfront_max_ttl}"
  }

  custom_error_response {
    error_caching_min_ttl = "${var.cloudfront_error_caching_min_ttl}"
    error_code            = "${var.cloudfront_error_code}"
    response_code         = "${var.cloudfront_response_code}"
    response_page_path    = "${var.cloudfront_response_page_path}"
  }

  origin {
    domain_name = "${var.cloudfront_origin_domain_name}"
    origin_id   = "${var.cloudfront_distribution_name}"
    origin_path = "${var.cloudfront_origin_path}"

    custom_origin_config {
      http_port                = "${var.cloudfront_origin_http_port}"
      https_port               = "${var.cloudfront_origin_https_port}"
      origin_protocol_policy   = "${var.cloudfront_origin_protocol_policy}"
      origin_ssl_protocols     = "${var.cloudfront_origin_ssl_protocols}"
      origin_keepalive_timeout = "${var.cloudfront_origin_keepalive_timeout}"
      origin_read_timeout      = "${var.cloudfront_origin_read_timeout}"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = "${var.acm_certificate_arn}"
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.1_2016"
  }

  restrictions {
    geo_restriction {
      restriction_type = "${var.cloudfront_geo_restriction_type}"
      locations        = "${var.cloudfront_geo_restriction_locations}"
    }
  }
}

data "aws_route53_zone" "default" {
  count   = "${var.route53_enabled == "true" ? signum(length(compact(var.cloudfront_aliases))) : 0}"
  zone_id = "${var.route53_parent_zone_id}"
  name    = "${var.route53_parent_zone_name}"
}

resource "aws_route53_record" "default" {
  count   = "${var.route53_enabled == "true" ? length(compact(var.cloudfront_aliases)) : 0}"
  zone_id = "${data.aws_route53_zone.default.zone_id}"
  name    = "${element(compact(var.cloudfront_aliases), count.index)}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.default.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.default.hosted_zone_id}"
    evaluate_target_health = "${var.route53_evaluate_target_health}"
  }
}
